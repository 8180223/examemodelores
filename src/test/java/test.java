
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class test {
    @Test
    public void initBoardBVA1(){
        BattleShip bs = new BattleShip();
        int[][] test = null;
        Assertions.assertThrows(Exception.class, () -> bs.initBoard(test),"Deveria dar input inválido");
    }
    @Test
    public void initBoardBVA2(){
        BattleShip bs = new BattleShip();
        int[][] test = new int[4][4];
        Assertions.assertThrows(Exception.class, () -> bs.initBoard(test),"Deveria dar input inválido");
    }
    @Test
    public void initBoardBVA3(){
        BattleShip bs = new BattleShip();
        int[][] test = new int[6][6];
        Assertions.assertThrows(Exception.class, () -> bs.initBoard(test),"Deveria dar input inválido");
    }
    @Test
    public void initBoardBVA4(){
        BattleShip bs = new BattleShip();
        int[][] test = new int[5][4];
        Assertions.assertThrows(Exception.class, () -> bs.initBoard(test),"Deveria dar input inválido");
    }
    @Test
    public void initBoardBVA5(){
        BattleShip bs = new BattleShip();
        int[][] test = new int[4][5];
        Assertions.assertThrows(Exception.class, () -> bs.initBoard(test),"Deveria dar input inválido");
    }
    @Test
    public void initBoardBVA6(){
        BattleShip bs = new BattleShip();
        int[][] test = new int[6][5];
        Assertions.assertThrows(Exception.class, () -> bs.initBoard(test),"Deveria dar input inválido");
    }
    @Test
    public void initBoardBVA7(){
        BattleShip bs = new BattleShip();
        int[][] test = new int[5][6];
        Assertions.assertThrows(Exception.class, () -> bs.initBoard(test),"Deveria dar input inválido");
    }
    @Test
    public void initBoardECP1() {
        BattleShip bs = new BattleShip();
        int[][] test = new int[5][5];
        bs.initBoard(test);
        boolean check = true;

        for (int i = 0; i < 5; i++) {
            for (int y = 0; y < 5; y++) {
                if (test[i][y] != -1) {
                    check = false;
                }
            }
        }

        Assertions.assertTrue(check, "A matriz não foi toda preenchida a -1");
    }
    @Test
    public void initBoardECP2(){
        BattleShip bs = new BattleShip();
        int[][] test = new int[3][3];
        Assertions.assertThrows(Exception.class, () -> bs.initBoard(test),"Deveria dar input inválido");
    }
    @Test
    public void initBoardECP3(){
        BattleShip bs = new BattleShip();
        int[][] test = new int[7][7];
        Assertions.assertThrows(Exception.class, () -> bs.initBoard(test),"Deveria dar input inválido");
    }
}
